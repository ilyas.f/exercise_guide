import React, { Component } from "react";
import SideBar from "../components/SideBar";
import ExerciseCard from "../components/ExerciseCard";
import Pagination from "../components/Pagination";
import GenderSelection from "../components/GenderSelection";

export default class ExerciseContainer extends Component {
  state = {
    filters: [],
    page: 1,
    male: true
  };

  setFilter = filter => {
    let filters = this.state.filters;
    const index = filters.indexOf(filter);
    if (index > -1) {
      filters.splice(index, 1);
    } else {
      filters.push(filter);
    }
    this.setState({ filters: filters, page: 1 });
  };

  clearFilters = () => {
    this.setState({ filters: [] });
  };

  filterExercises = () => {
    let exercises = this.props.exercises;
    let filters = this.state.filters;

    if (filters.length === 0) return exercises;

    return exercises.filter(exercise => {
      return exercise.bodyAreas.some(r => {
        return filters.includes(r);
      });
    });
  };

  paginateExercises = exercises => {
    let page = this.state.page;
    return exercises.slice((page - 1) * 8, page * 8);
  };

  setPageNumber = page => {
    this.setState({ page: page });
  };

  setGender = gender => {
    this.setState({ male: gender === "male" });
  };

  render() {
    const exercises = this.filterExercises();
    const exercisesToRender = this.paginateExercises(exercises);
    const pages = Math.ceil(exercises.length / 8);
    const { page, filters, male } = this.state;

    return (
      <>
        <div className="row mt-4">
          <GenderSelection setGender={this.setGender} />
          <Pagination
            page={page}
            pages={pages}
            setPageNumber={this.setPageNumber}
          />
        </div>
        <div className="row">
          <SideBar
            setFilter={this.setFilter}
            bodyAreas={this.props.bodyAreas}
            clearFilters={this.clearFilters}
          />
          <ExerciseCard
            filters={filters}
            exercises={exercisesToRender}
            male={male}
          />
        </div>
        <div className="row mt-4">
          <Pagination
            page={page}
            pages={pages}
            setPageNumber={this.setPageNumber}
            incrementPage={() => this.incrementPage}
            decrementPage={() => this.decrementPage}
          />
        </div>
      </>
    );
  }
}
