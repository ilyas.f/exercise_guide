import Layout from "../components/Layout";
import fetch from "isomorphic-unfetch";
import "../styles/sass/overrides.scss";
import ExerciseContainer from "../containers/ExerciseContainer";
import Loading from "../components/Loading";

export default class Exercises extends React.Component {
  state = {
    loading: true,
    bodyAreas: [],
    exercises: []
  };

  async componentDidMount() {
    const data = await fetch(
      "https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/"
    );
    const result = await data.json();
    let bodyAreas = new Set();
    result.exercises.map(e => {
      e.bodyAreas.map(b => bodyAreas.add(b));
    });
    this.setState({
      exercises: result.exercises,
      bodyAreas: Array.from(bodyAreas),
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <Layout>
        <div className="container-fluid">
          <ExerciseContainer
            bodyAreas={this.state.bodyAreas}
            exercises={this.state.exercises}
          />
        </div>
      </Layout>
    );
  }
}
