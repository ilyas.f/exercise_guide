import Layout from "../components/Layout";
import Link from "next/link";

const Home = () => {
  return (
    <Layout>
      <div className="jumbotron mt-3">
        <h1 className="display-4">Get your fit on!</h1>
        <p className="lead">
          Use the exercise guide to start your journey to a fitter you!
        </p>
        <hr className="my-4" />
        <p>Images and steps to help you exercise safely and effectively.</p>
        <p className="lead">
          <Link href="/exercises">
            <a className="btn btn-primary btn-lg" role="button">
              View exercises
            </a>
          </Link>
        </p>
      </div>
    </Layout>
  );
};

export default Home;
