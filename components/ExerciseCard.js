import { Fragment } from "react";
import React, { Component } from "react";

class ExerciseCard extends Component {
  state = {
    minHeight: "1px"
  };
  componentDidMount() {
    $(function() {
      $('[data-toggle="popover"]').popover();
    });
    this.matchHeight();
  }

  componentDidUpdate() {
    $(function() {
      $('[data-toggle="popover"]').popover();
    });
    this.matchHeight();
  }

  matchHeight() {
    var els = document.getElementsByClassName("card");
    var minHeight = 0;
    Array.prototype.forEach.call(els, function(el) {
      if (el.clientHeight > minHeight) minHeight = el.clientHeight;
    });
    if (this.state.minHeight !== minHeight)
      this.setState({ minHeight: minHeight });
  }

  render() {
    const { filters, exercises, male } = this.props;
    return (
      <div className="col">
        <h5>Showing: {filters.length ? filters.toString() : "All"}</h5>

        {exercises.map(exercise => {
          return (
            <Fragment key={exercise.id}>
              <div className="col-lg-6 col-md-12 d-inline-flex my-1">
                <div
                  className="card card-body p-1"
                  style={{ minHeight: `${this.state.minHeight}px` }}
                >
                  <div className="d-flex flex-row">
                    <div className="col-md-4 col-sm-3 p-0">
                      <img
                        className="img-fluid"
                        src={male ? exercise.male.image : exercise.female.image}
                      />
                    </div>
                    <div className="col-lg-9 col-md-8">
                      <h5 className="font-weight-bold">{exercise.name}</h5>
                      <p>Target area: {exercise.bodyAreas.join("/")}</p>

                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        data-container="body"
                        data-toggle="popover"
                        data-placement="right"
                        data-trigger="focus"
                        data-html="true"
                        data-content={exercise.transcript}
                      >
                        Show more
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          );
        })}
      </div>
    );
  }
}

export default ExerciseCard;
