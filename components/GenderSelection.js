const GenderSelection = ({ setGender }) => {
  return (
    <div className="row mb-3 col-lg-12 d-flex justify-content-center">
      I am:
      {["male", "female"].map((gender, index) => {
        return (
          <div key={index} className="custom-control custom-radio mx-3">
            <input
              type="radio"
              className="custom-control-input"
              id={`${gender}-radio`}
              name="genderRadios"
              defaultChecked={gender === "male" ? true : false}
              onClick={() => setGender(gender)}
            />
            <label className="custom-control-label" htmlFor={`${gender}-radio`}>
              {gender}
            </label>
          </div>
        );
      })}
    </div>
  );
};

export default GenderSelection;
