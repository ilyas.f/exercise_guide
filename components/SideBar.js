const SideBar = ({ setFilter, bodyAreas, clearFilters }) => {
  $(document).ready(function() {
    $(".clear-all").click(function() {
      let checkboxes = document.getElementsByClassName("filter-check");
      for (var i = 0; i < checkboxes.length; i++) {
        $(checkboxes[i]).prop("checked", false);
      }
    });
  });

  return (
    <div className="col-md-2 col-sm-12">
      <h5>Filter</h5>
      {bodyAreas.map((t, index) => {
        return (
          <div className="form-group filter-wrap" key={index}>
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input filter-check"
                id={t}
                defaultChecked=""
                onChange={() => setFilter(t)}
              />
              <label
                className="
              custom-control-label"
                htmlFor={t}
              >
                {t}
              </label>
            </div>
          </div>
        );
      })}
      <button
        type="button"
        className="btn btn-primary btn-sm clear-all"
        onClick={() => {
          clearFilters();
        }}
      >
        Clear all
      </button>
    </div>
  );
};

export default SideBar;
