import Layout from "./Layout";

const Loading = () => {
  return (
    <Layout>
      <div className="d-flex justify-content-center m-5 p-5">
        <button className="btn btn-primary" type="button" disabled>
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          ></span>
          Loading...
        </button>
      </div>
    </Layout>
  );
};

export default Loading;
