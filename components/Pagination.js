const Pagination = ({ page, pages, setPageNumber }) => {
  return (
    <div className="col d-flex justify-content-center">
      <nav aria-label="Page navigation">
        <ul className="pagination flex-wrap">
          <li className={`page-item border ${page === 1 ? "disabled" : ""}`}>
            <a
              className="page-link"
              href="#"
              onClick={() => setPageNumber(page - 1)}
              aria-label="Previous"
            >
              <span aria-hidden="true">&laquo;</span>
              <span className="sr-only">Previous</span>
            </a>
          </li>
          {Array.from(Array(pages)).map((a, index) => {
            return (
              <li
                key={index}
                className={`page-item border ${
                  index === page - 1 ? "active" : ""
                }`}
              >
                <a
                  className="page-link"
                  href="#"
                  onClick={() => setPageNumber(index + 1)}
                >
                  {index + 1}
                </a>
              </li>
            );
          })}
          <li
            className={`page-item border ${page === pages ? "disabled" : ""}`}
          >
            <a
              className="page-link"
              href="#"
              aria-label="Next"
              onClick={() => setPageNumber(page + 1)}
            >
              <span aria-hidden="true">&raquo;</span>
              <span className="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Pagination;
